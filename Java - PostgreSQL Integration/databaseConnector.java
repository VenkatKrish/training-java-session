import java.sql.*;

class DbConnectivity{

	private Connection getConnection(String password){
		Connection connectionObj = null;
		try{
			Class.forName("org.postgresql.Driver");
			connectionObj = DriverManager.getConnection("jdbc:postgresql://localhost:5432/applicationdatabase1","postgres", password);
		}
		catch(Exception e){
			e.printStackTrace();
			System.err.println(e.getClass().getName()+": "+e.getMessage());
         	System.exit(0);
		}

		return connectionObj;
	}

	public static void main(String ar[]){ 
		
		try{
			Connection connectionObj = new DbConnectivity().getConnection("Test@123");

			System.out.println("Opened Database Successfully");

			connectionObj.setAutoCommit(false);
			Statement statement = connectionObj.createStatement();
			String sql = "INSERT INTO TABLE1 (ID,NAME) "
	            + "VALUES (2, 'SUCHI');";
	        statement.executeUpdate(sql);
	        statement.close();
	        connectionObj.commit();
	        connectionObj.close();
	    }
	    catch(Exception e){
	    	e.printStackTrace();
	    	System.exit(0);
	    }

	    System.out.println("Values added succesfully!");
	}
}