package samplePackage;

public class MainClass{
	private int variable1;
	public int variable2;
	protected int variable3;

	public MainClass(int a,int b,int c){
		this.variable1 = a;
		this.variable2 = b;
		this.variable3 = c;
		this.display2();
		this.display3();
	}

	public MainClass(){
		System.out.println("Works!");
	}

	public void display(){
		System.out.println("Inside Public Method: "+""+variable1+" "+variable2+" "+variable3);
	}

	private void display2(){
		System.out.println("Inside Private Method: "+""+variable1+" "+variable2+" "+variable3);
	}

	protected void display3(){
		System.out.println("Inside protected Method: "+""+variable1+" "+variable2+" "+variable3);
	}
}