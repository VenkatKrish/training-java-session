import java.io.*;

class Solution{
	public static void main(String ar[]) throws FileNotFoundException{
		PrintStream fileWriter = new PrintStream(new File("output.txt"));
		PrintStream console = System.out;

		System.setOut(fileWriter);
		System.out.println("This line is written to a file!");
		System.out.println("Yep! this too");
		System.out.println("Even THis!");

		System.setOut(console);
		System.out.println("This wont!");
	}	
}